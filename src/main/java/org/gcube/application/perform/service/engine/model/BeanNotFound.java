package org.gcube.application.perform.service.engine.model;

public class BeanNotFound extends InternalException {

	public BeanNotFound() {
		// TODO Auto-generated constructor stub
	}

	public BeanNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BeanNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public BeanNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BeanNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
