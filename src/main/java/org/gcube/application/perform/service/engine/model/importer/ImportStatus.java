package org.gcube.application.perform.service.engine.model.importer;

public enum ImportStatus {
	ACCEPTED, RUNNING, COMPLETE, FAILED, CANCELLED
}
