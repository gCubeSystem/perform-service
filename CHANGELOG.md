This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Perform Service

## [v1.0.1-SNAPSHOT] 2020-09-03

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)